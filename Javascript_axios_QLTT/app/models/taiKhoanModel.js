const taiKhoanND = function (
    _taiKhoan,
    _hoTen,
    _matKhau,
    _email,
    _loaiND,
    _ngonNgu,
    _hinhAnh,
    _moTa
) {
    this.taiKhoan = _taiKhoan;
    this.hoTen = _hoTen;
    this.matKhau = _matKhau;
    this.email = _email;
    this.loaiND = _loaiND;
    this.ngonNgu = _ngonNgu;
    this.hinhAnh = _hinhAnh;
    this.moTa = _moTa
};
